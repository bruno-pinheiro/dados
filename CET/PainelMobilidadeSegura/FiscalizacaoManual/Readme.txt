------------------------------------------------------------------
PainelMobilidadeSegura_2014-2017_BaseIntegral.csv
------------------------------------------------------------------

Descrição:
- Fiscalização manual aplicada por agentes de trânsito
- Todas as infrações baixadas do site, inclusive as que o processo de geocoding não resultou Latitude e Longitude
- Latitude e Longitude no formato resultante do geocode

Dicionário:

Data - YYYY-MM-DD HH:MM:SS (Minutos e segundos sempre serão 00)
DiaSemana - dom|seg|ter|qua|qui|sex|sab
Depto - DSV|PM|GCM|SPTRANS (Órgão responsável pelo registro da infração)
CodVeiculo - 6 dígitos (Identificador único referente ao veículo que cometeu a infração)
Cod_enq - Código do enquadramento, segundo o Código de Trânsito Brasileiro
Enquadramento - Descrição do enquadramento, segundo o Código de Trânsito Brasileiro
QtdInfra - Soma das infrações registradas naquele local e horário
Local - Descrição do local onde fiscalização eletrônica estava instalada
Latitude - Latitude resultante do processo de Geocoding
Longitude - Longitude resultante do processo de Geocoding


------------------------------------------------------------------
PainelMobilidadeSegura_2014-2017_Cluster100Tableau_Publicacao.csv
------------------------------------------------------------------

Descrição:
- Fiscalização manual aplicada por agentes de trânsito
- Todas as infrações cujo processo de geocoding resultou em uma Latitude e uma Longitude
- Latitude e Longitude agrupadas em cluster de 100x100 metros, para visualização no Tableau

Dicionário:

Data - YYYY-MM-DD HH:MM:SS (Minutos e segundos sempre serão 00)
DiaSemana - dom|seg|ter|qua|qui|sex|sab
Depto - DSV|PM|GCM|SPTRANS (Órgão responsável pelo registro da infração)
PrefRegional - Subprefeitura onde ocorreu o registro da infração
Local - Descrição do local onde fiscalização eletrônica estava instalada
QtdInfra - Soma das infrações registradas naquele local e horário
Cod_enq - Código do enquadramento, segundo o Código de Trânsito Brasileiro
Enquadramento - Descrição do enquadramento, segundo o Código de Trânsito Brasileiro
Lat100 - Latitude em formato Tableau (float separado por vírgula), agrupada em um cluster de 100x100 metros
Long100 - Longitude em formato Tableau (float separado por vírgula), agrupada em um cluster de 100x100 metros


------------------------------------------------------------------
PainelMobilidadeSegura_2014-2017_Cluster100Tableau_PedCic.csv
------------------------------------------------------------------

Descrição:
- Fiscalização manual aplicada por agentes de trânsito
- Seleção de infrações mais relacionadas a colocar ciclistas e pedestres em risco
- Todas as infrações cujo processo de geocoding resultou em uma Latitude e uma Longitude
- Latitude e Longitude no formato resultante do geocode
- Latitude e Longitude agrupadas em cluster de 100x100 metros, para visualização no Tableau

Dicionário:

Data - YYYY-MM-DD HH:MM:SS (Minutos e segundos sempre serão 00)
Depto - DSV|PM|GCM|SPTRANS (Órgão responsável pelo registro da infração)
Cod_enq - Código do enquadramento, segundo o Código de Trânsito Brasileiro
Enquadramento - Descrição do enquadramento, segundo o Código de Trânsito Brasileiro
Risco_Ped_Cic - C|PC|P (Infrações que colocam maior risco a pedestres (P) e/ou ciclistas (C))
Agrupamento_PedCic - Agrupamento e simplificação do enquadramento, para facilitar filtragem
Parado_movimento - Parado|Em movimento (Categorização da infração)
QtdInfra - Soma das infrações registradas naquele local e horário
Local - Descrição do local onde fiscalização eletrônica estava instalada
Latitude - Latitude resultante do processo de Geocoding
Longitude - Longitude resultante do processo de Geocoding
Lat100 - Latitude em formato Tableau (float separado por vírgula), agrupada em um cluster de 100x100 metros
Long100 - Longitude em formato Tableau (float separado por vírgula), agrupada em um cluster de 100x100 metros
PrefRegional - Subprefeitura onde ocorreu o registro da infração
